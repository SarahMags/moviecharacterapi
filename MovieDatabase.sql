--
-- PostgreSQL database dump
--

-- Dumped from database version 13.1
-- Dumped by pg_dump version 13.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: character; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."character" (
    id bigint NOT NULL,
    character_alias character varying(255),
    character_full_name character varying(255),
    character_gender character varying(255),
    character_picture character varying(255)
);


ALTER TABLE public."character" OWNER TO postgres;

--
-- Name: character_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.character_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.character_id_seq OWNER TO postgres;

--
-- Name: character_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.character_id_seq OWNED BY public."character".id;


--
-- Name: character_movie; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.character_movie (
    character_id bigint NOT NULL,
    movie_id bigint NOT NULL
);


ALTER TABLE public.character_movie OWNER TO postgres;

--
-- Name: franchise; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.franchise (
    id bigint NOT NULL,
    franchise_description character varying(255),
    franchise_name character varying(255)
);


ALTER TABLE public.franchise OWNER TO postgres;

--
-- Name: franchise_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.franchise_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.franchise_id_seq OWNER TO postgres;

--
-- Name: franchise_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.franchise_id_seq OWNED BY public.franchise.id;


--
-- Name: movie; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.movie (
    id bigint NOT NULL,
    movie_director character varying(255),
    movie_genre character varying(255),
    movie_picture character varying(255),
    movie_title character varying(255),
    movie_trailer character varying(255),
    movie_year integer,
    franchise_id bigint
);


ALTER TABLE public.movie OWNER TO postgres;

--
-- Name: movie_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.movie_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.movie_id_seq OWNER TO postgres;

--
-- Name: movie_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.movie_id_seq OWNED BY public.movie.id;


--
-- Name: character id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."character" ALTER COLUMN id SET DEFAULT nextval('public.character_id_seq'::regclass);


--
-- Name: franchise id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.franchise ALTER COLUMN id SET DEFAULT nextval('public.franchise_id_seq'::regclass);


--
-- Name: movie id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie ALTER COLUMN id SET DEFAULT nextval('public.movie_id_seq'::regclass);


--
-- Data for Name: character; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (1, 'Snape', 'Severus Snape', 'male', 'https://en.wikipedia.org/wiki/Severus_Snape#/media/File:Ootp076.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (2, 'Ron', 'Ronald Weasley', 'male', 'https://upload.wikimedia.org/wikipedia/en/5/5e/Ron_Weasley_poster.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (3, 'Harry', 'Harry Potter', 'male', 'https://www.irishtimes.com/polopoly_fs/1.3170107.1501253408!/image/image.jpg_gen/derivatives/ratio_1x1_w1200/image.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (4, 'Hermoine', 'Hermoine Granger', 'female', 'https://i.pinimg.com/736x/34/0c/30/340c3024611f99b0f1cf20733058d314.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (6, 'Lord Voldemort', 'Tom Marvolo Riddle', 'male', 'https://upload.wikimedia.org/wikipedia/en/a/a3/Lordvoldemort.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (7, NULL, 'Newt Scamander', 'male', 'https://images-na.ssl-images-amazon.com/images/I/51e44uexIkL._AC_SX425_.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (5, NULL, 'Draco Malfoy', 'male', 'https://i.pinimg.com/originals/e2/b0/e7/e2b0e70f3627ce10d9faa701c9612148.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (8, 'Godfather, Don Vito Corleone', 'Vito Corleone', 'male', 'https://cdn.britannica.com/55/188355-050-D5E49258/Salvatore-Corsitto-The-Godfather-Marlon-Brando-Francis.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (9, 'Don Michael Corleone', 'Michael Corleone', 'male', 'https://cdn.britannica.com/55/188355-050-D5E49258/Salvatore-Corsitto-The-Godfather-Marlon-Brando-Francis.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (10, 'Joker', 'Arthur Fleck', 'male', 'https://psykologisk.no/wp-content/uploads/2019/12/joaquin_phoenix_joker.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (11, 'Harley Quinn', 'Harleen Frances Quinzel', 'female', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQl13aXJF3J8V4hYHN0cPRrmg7FKMLBf7wUww&usqp=CAU');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (12, 'Wonder Woman', 'Diana Prince', 'female', 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRNN3ORCkHY-_lPlGDyq0ZjxNOKqK5L3AvPHw&usqp=CAU');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (13, 'Kal-El, Superman', 'Clark Joseph Kent', 'male', 'https://m.media-amazon.com/images/M/MV5BODI0MTYzNTIxNl5BMl5BanBnXkFtZTcwNjg2Nzc0NA@@._V1_UY317_CR26,0,214,317_AL_.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (14, 'Wormie', 'Luke Skywalker', 'male', 'https://static.wikia.nocookie.net/starwars/images/3/3d/LukeSkywalker.png/revision/latest/scale-to-width-down/1000?cb=20201218190434');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (15, 'Iron Man', 'Tony Stark', 'male', 'https://upload.wikimedia.org/wikipedia/en/f/f2/Robert_Downey_Jr._as_Tony_Stark_in_Avengers_Infinity_War.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (16, 'God of Thunder', 'Thor Odinson', 'male', 'https://upload.wikimedia.org/wikipedia/en/3/3c/Chris_Hemsworth_as_Thor.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (17, 'Star-Lord', 'Peter Jason Quill', 'male', 'https://static.wikia.nocookie.net/marvel-cinematic-universe-au/images/f/f7/PeterQuillProfile.jpg/revision/latest?cb=20171103185343');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (18, 'Geri Halliwell', 'Ginger Spice', 'female', 'http://cdn.cnn.com/cnnnext/dam/assets/190701071936-02-geri-halliwell-ginger-spice-1997-brit-awards-restricted.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (19, 'Emma Bunton', 'Baby Spice', 'female', 'https://imgix.bustle.com/nylon/18444329/origin.jpg?w=1200&h=1000&fit=crop&crop=faces&auto=format%2Ccompress');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (20, 'Victoria Beckham', 'Posh Spice', 'female', 'https://cdn.newsapi.com.au/image/v1/dc83153892a4a798f175d7d756bbbcd9');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (21, 'Melanie Chisholm, Mel C', 'Sporty Spice', 'female', 'https://i.pinimg.com/originals/07/19/2e/07192e3821ec9c5ef7a4da852a935fce.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (22, 'Melanie Brown, Mel B', 'Scary Spice', 'female', 'https://i.pinimg.com/originals/eb/11/db/eb11db1e668515e83c929e2aaf22daf9.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (23, 'Gandalf the White, Olórin, Gandalf the Wandering Wizard', 'Gandalf the Grey', 'male', 'https://static.wikia.nocookie.net/lotr/images/8/8d/Gandalf-2.jpg/revision/latest/scale-to-width-down/340?cb=20130209172436');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (24, 'Spider-man', 'Peter Benjamin Parker', 'male', 'https://upload.wikimedia.org/wikipedia/en/2/21/Web_of_Spider-Man_Vol_1_129-1.png');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (25, '007', 'James Bond', 'male', 'https://media.gq-magazine.co.uk/photos/5d1394634858d3440a004d21/16:9/w_1920,c_limit/Goldfinger-hp-GQ-07Apr15_rex_b.jpg');
INSERT INTO public."character" (id, character_alias, character_full_name, character_gender, character_picture) VALUES (26, 'B-127', 'Bumblebee', 'debatable', 'https://img1.looper.com/img/gallery/the-ending-of-bumblebee-explained/intro-1545680472.jpg');


--
-- Data for Name: character_movie; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.character_movie (character_id, movie_id) VALUES (1, 4);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (1, 3);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (1, 1);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (2, 1);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (2, 3);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (2, 4);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (3, 3);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (3, 1);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (3, 4);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (4, 3);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (4, 4);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (4, 1);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (5, 1);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (5, 4);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (5, 3);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (6, 1);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (6, 4);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (6, 3);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (7, 2);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (8, 6);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (8, 5);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (9, 6);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (9, 7);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (9, 5);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (10, 8);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (11, 9);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (12, 10);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (13, 11);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (14, 12);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (14, 13);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (15, 14);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (16, 15);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (17, 16);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (18, 17);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (19, 17);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (20, 17);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (21, 17);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (22, 17);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (23, 19);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (23, 18);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (24, 21);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (24, 20);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (25, 22);
INSERT INTO public.character_movie (character_id, movie_id) VALUES (26, 23);


--
-- Data for Name: franchise; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (1, 'An organized crime dynasty''s aging patriarch transfers control of his clandestine empire to his reluctant son.', 'The Godfather');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (2, 'In addition to the eight Harry Potter movies, Warner Bros. launched a series of films that explore the larger Wizarding World, known as Fantastic Beasts series.', 'The Wizarding World');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (3, 'The DC Extended Universe franchise has become one of the highest-grossing media franchises.', 'DC Extended Universe');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (4, 'In 1977, Star Wars captured the imagination of a generation. More than 40 years later, the franchise continues to dominate the box office.', 'Star Wars');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (5, 'The Marvel Cinematic Universe is one of the most prolific franchises in cinema history.', 'Marvel Cinematic Universe');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (6, 'The Spice Girls'' success include international record sales, vast merchandising enterprises, iconic symbolism and a film: Spice World.', 'Spice Girls');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (7, 'Based on the writings of J.R.R. Tolkien, there are six films that make up the Middle Earth film franchise.', 'Middle Earth');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (8, 'Spider-man has appeared in seven solo live-action films since 2002. Spider-man has been portrayed by three separate actors. ', 'Spider-Man');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (9, 'James Bond is perhaps one of the longest-running film franchises in cinematic history. There has been made 24 James Bond films since 1963.', 'James Bond');
INSERT INTO public.franchise (id, franchise_description, franchise_name) VALUES (10, 'Based on Hasbro''s hit toy line, Transformers has become a successful franchise for paramount over the last two decades.', 'Transformers');


--
-- Data for Name: movie; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (1, ' David Yates', 'Adventure', 'https://www.imdb.com/title/tt0417741/mediaviewer/rm282560512/', 'Half-Blood Prince', 'https://www.imdb.com/video/vi1061421849?playlistId=tt0417741&ref_=tt_ov_vi', 2009, 2);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (2, ' David Yates', 'Adventure', 'https://m.media-amazon.com/images/M/MV5BMjMxOTM1OTI4MV5BMl5BanBnXkFtZTgwODE5OTYxMDI@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Fantastic Beasts and Where to Find Them', 'https://www.youtube.com/watch?v=MtjfNzEVfAQ', 2016, 2);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (3, ' David Yates', 'Adventure', 'https://m.media-amazon.com/images/M/MV5BMGVmMWNiMDktYjQ0Mi00MWIxLTk0N2UtN2ZlYTdkN2IzNDNlXkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Harry Potter and the Deathly Hallows: Part 1', 'https://www.youtube.com/watch?v=mObK5XD8udk', 2011, 2);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (4, ' Chris Columbus', 'Adventure', 'https://www.imdb.com/title/tt0241527/mediaviewer/rm683213568/', 'Harry Potter and the Sorcerers Stone', 'https://www.youtube.com/watch?v=Yki6KgoBstM', 2001, 2);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (5, ' Francis Ford Coppola', 'Crime, Drama', 'https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg', 'The Godfather', 'https://www.imdb.com/video/imdb/vi1348706585?playlistId=tt0068646&ref_=tt_ov_vi', 1972, 1);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (6, ' Francis Ford Coppola', 'Crime, Drama', 'https://m.media-amazon.com/images/M/MV5BMWMwMGQzZTItY2JlNC00OWZiLWIyMDctNDk2ZDQ2YjRjMWQ0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UY268_CR3,0,182,268_AL_.jpg', 'The Godfather: Part II', 'https://www.imdb.com/video/imdb/vi696162841?playlistId=tt0071562&ref_=tt_ov_vi', 1974, 1);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (7, ' Francis Ford Coppola', 'Crime, Drama', 'https://m.media-amazon.com/images/M/MV5BNWFlYWY2YjYtNjdhNi00MzVlLTg2MTMtMWExNzg4NmM5NmEzXkEyXkFqcGdeQXVyMDk5Mzc5MQ@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'The Godfather: Part III', 'https://www.imdb.com/video/imdb/vi1237827865?playlistId=tt0099674&ref_=tt_ov_vi', 1990, 1);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (8, 'Todd Phillips', 'Crime, Drama, Thriller', 'https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Joker', 'https://www.imdb.com/video/imdb/vi1723318041?playlistId=tt7286456&ref_=tt_ov_vi', 2019, 3);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (9, 'Cathy Yan', 'Action, Adventure, Comedy', 'https://m.media-amazon.com/images/M/MV5BMzQ3NTQxMjItODBjYi00YzUzLWE1NzQtZTBlY2Y2NjZlNzkyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Birds of Prey (and the Fantabulous Emancipation of One Harley Quinn)', 'https://www.imdb.com/video/imdb/vi3883843353?playlistId=tt7713068&ref_=tt_ov_vi', 2020, 3);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (10, 'Patty Jenkins', 'Action, Adventure, Fantasy', 'https://m.media-amazon.com/images/M/MV5BMTYzODQzYjQtNTczNC00MzZhLTg1ZWYtZDUxYmQ3ZTY4NzA1XkEyXkFqcGdeQXVyODE5NzE3OTE@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Wonder Woman', 'https://www.imdb.com/video/imdb/vi3944268057?playlistId=tt0451279&ref_=tt_ov_vi', 2017, 3);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (11, 'Zack Snyder', 'Action, Adventure, Sci-Fi', 'https://m.media-amazon.com/images/M/MV5BMTk5ODk1NDkxMF5BMl5BanBnXkFtZTcwNTA5OTY0OQ@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Man of Steel', 'https://www.imdb.com/video/imdb/vi705668633?playlistId=tt0770828&ref_=tt_ov_vi', 2013, 3);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (12, 'George Lucas', 'Action, Adventure, Fantasy', 'https://m.media-amazon.com/images/M/MV5BNzVlY2MwMjktM2E4OS00Y2Y3LWE3ZjctYzhkZGM3YzA1ZWM2XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Star Wars', 'https://www.imdb.com/video/imdb/vi1317709849?playlistId=tt0076759&ref_=tt_ov_vi', 1977, 4);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (13, 'Irvin Kershner', 'Action, Adventure, Fantasy', 'https://m.media-amazon.com/images/M/MV5BYmU1NDRjNDgtMzhiMi00NjZmLTg5NGItZDNiZjU5NTU4OTE0XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Star Wars: Episode V - The Empire Strikes Back', 'https://www.imdb.com/video/imdb/vi221753881?playlistId=tt0080684&ref_=tt_ov_vi', 1980, 4);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (14, 'Jon Favreau', 'Action, Adventure, Sci-Fi', 'https://m.media-amazon.com/images/M/MV5BMTczNTI2ODUwOF5BMl5BanBnXkFtZTcwMTU0NTIzMw@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Iron Man', 'https://www.imdb.com/video/imdb/vi447873305?playlistId=tt0371746&ref_=tt_ov_vi', 2008, 5);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (15, 'Kenneth Branagh', 'Action, Adventure, Fantasy', 'https://m.media-amazon.com/images/M/MV5BOGE4NzU1YTAtNzA3Mi00ZTA2LTg2YmYtMDJmMThiMjlkYjg2XkEyXkFqcGdeQXVyNTgzMDMzMTg@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Thor', 'https://www.imdb.com/video/imdb/vi1431476761?playlistId=tt0800369&ref_=tt_ov_vi', 2011, 5);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (16, 'James Gunn', 'Action, Adventure, Comedy', 'https://m.media-amazon.com/images/M/MV5BMTAwMjU5OTgxNjZeQTJeQWpwZ15BbWU4MDUxNDYxODEx._V1_UX182_CR0,0,182,268_AL_.jpg', 'Guardians of the Galaxy', 'https://www.imdb.com/video/imdb/vi1441049625?playlistId=tt2015381&ref_=tt_ov_vi', 2014, 5);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (17, 'Bob Spiers', 'Comedy, Family, Music', 'https://m.media-amazon.com/images/M/MV5BY2FiZjA1ODQtOWNjMC00ZmM4LThiNjktZmQ3YjBhMGQ2NzI2XkEyXkFqcGdeQXVyODU4NTYwMDA@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Spice World', 'https://www.imdb.com/video/imdb/vi747241753?playlistId=tt0120185&ref_=tt_ov_vi', 1997, 6);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (18, 'Peter Jackson', 'Action, Adventure, Drama', 'https://m.media-amazon.com/images/M/MV5BN2EyZjM3NzUtNWUzMi00MTgxLWI0NTctMzY4M2VlOTdjZWRiXkEyXkFqcGdeQXVyNDUzOTQ5MjY@._V1_UX182_CR0,0,182,268_AL_.jpg', 'The Lord of the Rings: The Fellowship of the Ring', 'https://www.imdb.com/video/imdb/vi2073101337?playlistId=tt0120737&ref_=tt_ov_vi', 2001, 7);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (19, 'Peter Jackson', 'Adventure, Fantasy', 'https://m.media-amazon.com/images/M/MV5BMTcwNTE4MTUxMl5BMl5BanBnXkFtZTcwMDIyODM4OA@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'The Hobbit: An Unexpected Journey', 'https://www.imdb.com/video/imdb/vi650683417?playlistId=tt0903624&ref_=tt_ov_vi', 2012, 7);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (20, 'Sam Raimi', 'Action, Adventure, Sci-Fi', 'https://m.media-amazon.com/images/M/MV5BZDEyN2NhMjgtMjdhNi00MmNlLWE5YTgtZGE4MzNjMTRlMGEwXkEyXkFqcGdeQXVyNDUyOTg3Njg@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Spider-Man', 'https://www.imdb.com/video/imdb/vi422553113?playlistId=tt0145487&ref_=tt_ov_vi', 2002, 8);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (21, 'Jon Watts', 'Action, Adventure, Sci-Fi', 'https://m.media-amazon.com/images/M/MV5BMGZlNTY1ZWUtYTMzNC00ZjUyLWE0MjQtMTMxN2E3ODYxMWVmXkEyXkFqcGdeQXVyMDM2NDM2MQ@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Spider-Man: Far from Home', 'https://www.imdb.com/video/imdb/vi2600385561?playlistId=tt6320628&ref_=tt_ov_vi', 2019, 8);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (22, 'Guy Hamilton', 'Action, Adventure, Thriller', 'https://m.media-amazon.com/images/M/MV5BMTQ2MzE0OTU3NV5BMl5BanBnXkFtZTcwNjQxNTgzNA@@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Goldfinger', 'https://www.imdb.com/video/imdb/vi2383218457?playlistId=tt0058150&ref_=tt_ov_vi', 1964, 9);
INSERT INTO public.movie (id, movie_director, movie_genre, movie_picture, movie_title, movie_trailer, movie_year, franchise_id) VALUES (23, 'Michael Bay', 'Action, Adventure, Sci-Fi', 'https://m.media-amazon.com/images/M/MV5BNDg1NTU2OWEtM2UzYi00ZWRmLWEwMTktZWNjYWQ1NWM1OThjXkEyXkFqcGdeQXVyMTQxNzMzNDI@._V1_UX182_CR0,0,182,268_AL_.jpg', 'Transformers', 'https://www.imdb.com/video/imdb/vi513779993?playlistId=tt0418279&ref_=tt_ov_vi', 2007, 10);


--
-- Name: character_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.character_id_seq', 26, true);


--
-- Name: franchise_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.franchise_id_seq', 10, true);


--
-- Name: movie_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.movie_id_seq', 23, true);


--
-- Name: character_movie character_movie_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.character_movie
    ADD CONSTRAINT character_movie_pkey PRIMARY KEY (character_id, movie_id);


--
-- Name: character character_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."character"
    ADD CONSTRAINT character_pkey PRIMARY KEY (id);


--
-- Name: franchise franchise_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.franchise
    ADD CONSTRAINT franchise_pkey PRIMARY KEY (id);


--
-- Name: movie movie_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT movie_pkey PRIMARY KEY (id);


--
-- Name: character_movie fkcww072p9tq9k4den5ci54a3eb; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.character_movie
    ADD CONSTRAINT fkcww072p9tq9k4den5ci54a3eb FOREIGN KEY (movie_id) REFERENCES public.movie(id);


--
-- Name: movie fke6bia0emn9rgc6498fspevei; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.movie
    ADD CONSTRAINT fke6bia0emn9rgc6498fspevei FOREIGN KEY (franchise_id) REFERENCES public.franchise(id);


--
-- Name: character_movie fkeea614wcon5ebvxen5vtj2pr7; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.character_movie
    ADD CONSTRAINT fkeea614wcon5ebvxen5vtj2pr7 FOREIGN KEY (character_id) REFERENCES public."character"(id);


--
-- PostgreSQL database dump complete
--

