package no.noroff.MovieCharacterAPI.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Franchise;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.repositories.FranchiseRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1")
public class FranchiseController {
    private final FranchiseRepository franchiseRepository;

    public FranchiseController(FranchiseRepository franchiseRepository) {
        this.franchiseRepository = franchiseRepository;
    }

    /**
     * This getmapping returns a list of all franchises in the database,
     * together with the status code 200-OK.
     * If the list is empty the status 204- no content is returned.
     * */
    @Operation(summary = "Get all franchises")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing all franchises",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Franchise.class)))}) })
    @GetMapping("/franchises")
    public ResponseEntity<List<Franchise>> getAllFranchise() {
        HttpStatus status;
        List<Franchise> franchises = franchiseRepository.findAll();
        if (franchises.size() == 0) {
            status = HttpStatus.NO_CONTENT;
        } else {
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(franchises, status);
    }

    /**
     * This finds a specific franchise based on the path variable id. If the franchise is found,
     * it is returned together with the status code 200-OK.
     * If the franchise does not exists, the status code 404 - Not found is returned.
     * */

    @Operation(summary = "Get a franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing the franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @GetMapping("/franchise/{id}")
    public ResponseEntity<Franchise> getSpecificFranchise(@PathVariable Long id) {
        Franchise returnFranchise = new Franchise();
        HttpStatus status;
        if(franchiseRepository.existsById(id)){
            status = HttpStatus.OK;
            returnFranchise = franchiseRepository.findById(id).get();
        }else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(returnFranchise, status);

    }

    /**
     * This postmapping takes in a franchise and adds it to the database.
     * It returns the newly added franchise, together with the status code 201-created.
     * */

    @Operation(summary = "Add a franchise")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created franchise",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class)) }) })
    @PostMapping("/franchise")
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise){
        Franchise returnFranchise = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnFranchise,status);
    }

    /**
     * This updates an existing franchise in the database.
     * It takes in an id together with a franchise object containing the updated information.
     * If the id in the path variable does not match the id in the request body,
     * the status code 400 - bad request is returned.
     * If the id's match the franchise is updated and returned together with status 204 no content.
     * */

    @Operation(summary = "Update an existing franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated franchise",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content) })
    @PutMapping("/franchise/{id}")
    public ResponseEntity<Franchise> updateFranchise(@PathVariable Long id, @RequestBody Franchise franchise){
        Franchise returnFranchise = new Franchise();
        HttpStatus status;

        if(!id.equals(franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnFranchise,status);
        }
        returnFranchise = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnFranchise,status);
    }

    /**
     * Get mapping that returns all movies in a specific franchise.
     * It takes in a franchise id, and if the franchise exists it will return all movies in this franchise,
     * together with the status 200 - ok.
     * If the franchise does not exists it will return null, together with the status code 404 - not found.
     * */

    @Operation(summary = "Get all movies by franchise id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing all movies",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Movie.class)))}),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @GetMapping("/franchise/{id}/movies")
    public ResponseEntity<Set<Movie>> getMoviesByFranchiseID(@PathVariable Long id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        if (franchiseRepository.existsById(id)) {
            Set<Movie> returnMovies = franchise.getMovies();
            HttpStatus status = HttpStatus.OK;
            return new ResponseEntity<>(returnMovies, status);
        } else {
            HttpStatus status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }
    }

    /**
     * This returns a list of all characters in a specific franchise. To get the characters,
     * it first gets a list of all movies in the franchise. It loops trough this list to get all characters.
     * If the character is already listed (plays in more than one movie in the franchise),
     * it is not added to the list again.
     * The list is returned together with status 200 - ok.
     * If the franchise does not exists, null and the status 404 - not found is returned
     * */
    @Operation(summary = "Get all characters by franchise id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing all characters",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Character.class)))}),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @GetMapping("/franchise/{id}/characters")
    public ResponseEntity<List<Character>> getCharactersByFranchiseID(@PathVariable Long id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        if (franchiseRepository.existsById(id)) {
            Set<Movie> movies = franchise.getMovies();
            List<Character> characters = new ArrayList<Character>();

            for (Movie movie : movies) {
                Set<Character> characters1 = movie.getCharacters();
                for (Character character : characters1) {
                    if (!characters.contains(character)){
                        characters.add(character);
                    }
                }
            }
            HttpStatus status = HttpStatus.OK;
            return new ResponseEntity<>(characters, status);
        } else {
            HttpStatus status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }
    }

    /**
     * This deletes a franchise from the database. First it loops trough all movies in the franchise,
     * and set the franchise object in the movie to null.
     * Then it deletes the franchise and returns the status 200 - ok.
     * If the franchise does not exists, the status 404- not found is returned.
     * */
    @Operation(summary = "Delete franchise by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Franchise deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Franchise not found",
                    content = @Content) })
    @DeleteMapping("/franchise/{id}")
    public ResponseEntity<Franchise> deleteFranchise(@PathVariable Long id){
        HttpStatus status;

        if(franchiseRepository.existsById(id)){
            Franchise franchise = franchiseRepository.findById(id).get();
            for (Movie movie : franchise.getMovies()){
                movie.setFranchise(null);
            }
            franchiseRepository.delete(franchise);
            status = HttpStatus.OK;
        }
        else{
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(status);
    }
}


