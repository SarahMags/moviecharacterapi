package no.noroff.MovieCharacterAPI.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import no.noroff.MovieCharacterAPI.models.Character;
import no.noroff.MovieCharacterAPI.models.Franchise;
import no.noroff.MovieCharacterAPI.models.Movie;
import no.noroff.MovieCharacterAPI.repositories.FranchiseRepository;
import no.noroff.MovieCharacterAPI.repositories.MovieRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("api/v1")
public class MovieController {
    private final MovieRepository movieRepository;

    public MovieController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    /**
     * This finds a specific movie based on the path variable id. If the movie is found,
     * it is returned together with the status code 200-OK.
     * If the movie does not exists in the database, the status code 404 - Not found is returned.
     * */
    @Operation(summary = "Get a movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing the movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @GetMapping("/movie/{id}")
    public ResponseEntity<Movie> getSpecificMovie(@PathVariable Long id){
        HttpStatus status = HttpStatus.NOT_FOUND;
        Movie returnMovie = null;
        boolean exists = movieRepository.existsById(id);
        if (exists){
            returnMovie = movieRepository.findById(id).get();
            status = HttpStatus.OK;
        }
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * This updates an existing movie in the database.
     * It takes in an id together with a movie object with the updated information.
     * If the id in the path variable does not match the id in the request body,
     * the status code 400 - bad request is returned.
     * If the id's match the movie is updated and returned together with status 204 no content.
     * */
    @Operation(summary = "Update an existing movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Updated movie",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content) })
    @PutMapping("/movie/{id}")
    public ResponseEntity<Movie> updateExistingMovie(@PathVariable Long id, @RequestBody Movie movie){
        Movie returnMovie = new Movie();
        HttpStatus status;

        if(!id.equals(movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnMovie,status);
        }
        returnMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnMovie, status);
    }

    /**
     * This get mapping returns a list of all movies in the database.
     * Together with the status code 200-OK.
     * */
    @Operation(summary = "Get all movies")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing all movies",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Movie.class)))}) })
    @GetMapping("/movies")
    public ResponseEntity<List<Movie>> getAllMovies(){
        List<Movie> movies = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(movies,status);
    }

    /**
     * This post mapping takes in a movie and adds it to the database.
     * It returns the newly added movie, together with the status code 201-created.
     * */

    @Operation(summary = "Add a movie")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Created movie",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class)) }) })
    @PostMapping("/movie")
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie){
        movie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(movie, status);
    }

    /**
     * This returns the list of characters in a specific movie.
     * It takes in a movie id, if the movie exists in the database.
     * It gets all characters in the movie and returns them together with status 200-ok.
     * If the movie does not exists in the database, it returns 404 not found and null.
     * */
    @Operation(summary = "Get all characters by movie id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Showing all characters",
                    content = { @Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = Character.class)))}),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @GetMapping("/movie/{id}/characters")
    public ResponseEntity<Set<Character>> getCharactersByMovieID(@PathVariable Long id) {
        Movie movie = movieRepository.findById(id).get();
        Set<Character> characters = null;

        if (movieRepository.existsById(id)) {
            characters = movie.getCharacters();
            HttpStatus status = HttpStatus.OK;
            return new ResponseEntity<>(characters, status);
        } else {
            HttpStatus status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }
    }

    /**
     * This deletes a movie from the database.
     * First it removes the movie from the franchise.
     * Then it removes the movie from each character that plays in this movie.
     * It returns status 200- ok when the movie is deleted.
     * If the movie does not exists, status 404- not found is returned.
     * */
    @Operation(summary = "Delete movie by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Movie deleted",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Movie not found",
                    content = @Content) })
    @DeleteMapping("/movie/{id}")
    public ResponseEntity<Movie> deleteMovie(@PathVariable Long id){

        HttpStatus status;
        Movie movie = movieRepository.findById(id).get();

        if(movieRepository.existsById(id)){
            movie.getFranchise().getMovies().remove(movie);
            for(Character character: movie.getCharacters()){
                character.getMovies().remove(movie);
            }
            movieRepository.delete(movie);
            status = HttpStatus.OK;
        }
        else {
            status = HttpStatus.NOT_FOUND;
        }
        return new ResponseEntity<>(null, status);
    }


}
